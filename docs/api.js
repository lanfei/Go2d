YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Class",
        "Director",
        "DisplayObject",
        "Ease",
        "Event",
        "EventEmitter",
        "ImageView",
        "Matrix",
        "ObjectPool",
        "ResizeEvent",
        "ResourceLoader",
        "ScrollEvent",
        "ScrollView",
        "Sprite",
        "Stage",
        "TextField",
        "TouchEvent",
        "Tween",
        "URLRequest",
        "Vector",
        "go2d"
    ],
    "modules": [
        "go2d"
    ],
    "allModules": [
        {
            "displayName": "go2d",
            "name": "go2d",
            "description": "Go2d 命名空间"
        }
    ]
} };
});
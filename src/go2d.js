/**
 * Go2d 命名空间
 * @module go2d
 * @class go2d
 * @property {string} version Go2d 版本号
 */
var go2d = {
	version: '@VERSION'
};
